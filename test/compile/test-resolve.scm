;; test-resolve.scm: resolver test routines for r6rs-thrift
;; Copyright (C) 2012 Julian Graham

;; r6rs-thrift is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(import (rnrs))
(import (srfi :64))
(import (thrift compile conditions))
(import (thrift compile parse))
(import (thrift compile resolve))
(import (thrift private))

(define (primitive-ref descriptor)
  (thrift:make-type-reference 
   (thrift:field-type-descriptor-name descriptor) #f descriptor))

(test-begin "resolve")
(test-begin "service")

(test-group "throws"
  (let* ((function-definition 
	  (thriftc:make-function-definition 
	   "throwException" (primitive-ref thrift:field-type-void) '() 
	   (list (primitive-ref thrift:field-type-bool)) #f))
	 (service-definition 
	  (thriftc:make-service-definition 
	   "TestService" #f (list function-definition)))
	 (thrift (thriftc:make-thrift '() '() (list service-definition))))
    (call/cc
     (lambda (cc)
       (with-exception-handler
	(lambda (condition)
	  (test-assert (thriftc:type-resolution-condition? condition))
	  (cc))
	(lambda ()
	  (thriftc:resolve thrift)
	  (raise (make-assertion-violation))))))))

(test-end "service")
(test-end "resolve")
