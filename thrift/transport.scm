;; transport.scm: base type declarations for Thrift transport implementations
;; Copyright (C) 2012 Julian Graham

;; r6rs-thrift is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (thrift transport)
  (export thrift:transport
	  thrift:make-transport
	  thrift:transport?

	  thrift:transport-input-port
	  thrift:transport-output-port)
  (import (rnrs records syntactic))

  (define-record-type (thrift:transport 
		       thrift:make-transport 
		       thrift:transport?)
    (fields input-port output-port))
)
