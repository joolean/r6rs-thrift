;; conditions.scm: Condition types for the r6rs-thrift compiler
;; Copyright (C) 2012 Julian Graham

;; r6rs-thrift is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (thrift compile conditions)
  (export &thriftc:location
	  thriftc:location-condition?
	  thriftc:make-location-condition
	  thriftc:location-condition-location

	  &thriftc:type-resolution
	  thriftc:type-resolution-condition?
	  thriftc:make-type-resolution-condition)
  (import (rnrs))

  (define-condition-type &thriftc:location &condition 
    thriftc:make-location-condition thriftc:location-condition? 
    (location thriftc:location-condition-location))

  (define-condition-type &thriftc:type-resolution &condition
    thriftc:make-type-resolution-condition thriftc:type-resolution-condition?)
)
