Thrift for R6RS Scheme
======================

What is it?
-----------

This project provides a pure Scheme implementation of the Apache Thrift framework, including parsing and code generation. Visit the Apache Thrift [project page](https://thrift.apache.org/) for information about the Thrift definition language and the various Thrift transports and wire protocols.

Discussion
----------

Visit the [discussion group](https://groups.google.com/forum/#!forum/r6rs-thrift).

Documentation
-------------

Read the [documentation](https://gitlab.com/joolean/r6rs-thrift/blob/master/HOWTO).

Quick Example
-------------

You write a .thrift file like this

    namespace rnrs thrift.person;

    struct Person {
      1: required int32 id;
      2: required string name;
      3: optional string email;
    }

and compile it like this

    (import (thrift compile))
    (define thrift (thriftc:read-thrift (open-input-file "Person.thrift")))
    (thriftc:generate-library thrift)

to produce an R6RS library form like this

    (library (thrift person)
      (export make-Person-builder     
              Person-builder?
              Person-builder-build
          
              Person-builder-id
              set-Person-builder-id!
              has-Person-builder-id?
              clear-Person-builder-id!

              Person-builder-name
              set-Person-builder-name!
              has-Person-builder-name?
              clear-Person-builder-name!

              Person-builder-email
              set-Person-builder-email!
              has-Person-builder-email?
              clear-Person-builder-email!

              Person?
              Person-id
              Person-name
              Person-email
              has-Person-email?

              Person-read
              Person-write)

      (import (rnrs base)
              (rnrs enums)
              (rnrs records syntactic)
              (thrift private))

      ...
    )
